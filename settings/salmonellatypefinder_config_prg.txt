# Configuration file for the MLST and SeqSero typer software
python3	/usr/bin/python3
python	/opt/conda/bin/python2
srst2	/usr/src/cgepipeline/services/salmonellatypefinder/dependencies/srst2.py
seqsero	/usr/src/cgepipeline/services/salmonellatypefinder/dependencies/SeqSero/SeqSero.py
bowtie2	/usr/src/bowtie2-2.3.3.1/bowtie2
bowtie2_dir	/usr/src/bowtie2-2.3.3.1
bwa	/usr/src/bwa-0.7.16a
samtools	/usr/local/bin/samtools
samtools_dir	/usr/src/samtools-1.6
blastn	/usr/bin/blastn
makeblastdb	/usr/bin/makeblastdb
hostname	/bin/hostname
chmod	/bin/chmod
