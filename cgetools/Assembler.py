#!/usr/bin/env python3
'''
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
'''
import sys, os, json
from cgecore import (check_file_type, debug, get_arguments, proglist, Program,
                     mkpath, copy_file)

# SET GLOBAL VARIABLES
service, version = "Assembler", "2.0"

def main():
   ''' MAIN '''
   # PARSE ARGUMENTS
   # Add service specific arguments using the following format:
   #  (OPTION, VARIABLE,  DEFAULT,  HELP)
   args = get_arguments([
      ('--trim', 'trim_reads', False, ("write 'trim' for trimming of reads, "
                                        "else write '' (default='trim')")),
      ('--sequencing_platform', 'sequencing_platform', None,
       ("Sequencing Platform")),
      ('--sequencing_type', 'sequencing_type', None, ("Sequencing Type")),
      ('--files', 'files', None, ("raw unassembled fastq files"))
      ])
   
   # VALIDATE REQUIRED ARGUMENTS
   if args.files is None or len(args.files) == 0:
      debug.graceful_exit("Input Error: No files were provided!\n")
   files = []
   args.files = args.files.split(',')
   for fil in args.files:
      if not os.path.exists(fil):
         debug.graceful_exit("Input Error: The specified file '%s' does not "
                             "exist!\n"%(fil))
      elif check_file_type(fil) != 'fastq':
         debug.graceful_exit(('Input Error: Invalid file format (%s)!\nOnly the'
                              ' fastq format is recognised as a proper format.'
                              '\n')%(check_file_type(fil)))
      files.append(os.path.abspath(fil))
   
   if args.sequencing_platform is None:
      debug.graceful_exit("Input Error: No sequencing platform was specified!"
                          "\n")
   elif args.sequencing_type is None:
      debug.graceful_exit("Input Error: No sequencing type was specified!\n")
   
   # Convert sequencing information to our sequencing schemas
   # (plat, type): (seq_tech, tech_setup_method)
   technology_dict = {
      ('unknown','unknown'): ("unknown", None),
      ('unknown','single'): ("unknown", None),
      ('unknown','paired'): ("unknown", None),
      ('Illumina','single'): ("Illumina", set_illumina_cmd),
      ('Illumina','paired'): ("Paired_End_Reads",  set_illumina_cmd),
      ('Illumina','unknown'): ("unknown", None),
      ('LS454','single'): ("454", set_454_cmd),
      ('LS454','paired'): ("454_Paired_End_Reads", set_454_cmd),
      ('LS454','unknown'): ("unknown", None),
      ('ABI SOLiD','single'): ("Solid", set_solid_cmd),
      ('ABI SOLiD','paired'): ("S_Paired_End_Reads", set_solid_cmd),
      ('ABI SOLiD','mate-paired'): ("S_Mate_Paired_Reads", set_solid_cmd),
      ('ABI SOLiD','unknown'): ("unknown", None),
      ('Ion Torrent','single'): ("Ion_Torrent", set_ion_cmd),
      ('Ion Torrent','unknown'): ("unknown", None)
   }
   try: seq_tech, tech_setup = technology_dict[(
      args.sequencing_platform,args.sequencing_type)]
   except: debug.graceful_exit('Input Error: The sequencing platform or type '
                               'was not recognised!')
   
   # Check whether seq_tech is known
   debug.log(seq_tech)
   if seq_tech == 'unknown':
      debug.graceful_exit("Input Error: We cannot assemble sequences from "
                          "unkown platforms!\n")
   
   # Checking if the correct number of files were submitted
   check_correct_number_of_files(len(files), seq_tech)
   
   # Create assembly dir
   mkpath('output')
   
   # Set output file path
   if seq_tech in ["454", "Ion_Torrent"]:
      output_file = "output/denovo/assembly/454LargeContigs.fna"
   elif seq_tech == "454_Paired_End_Reads":
      output_file = "output/denovo/assembly/454Scaffolds.fna"
   elif seq_tech == "Illumina" or seq_tech == "Paired_End_Reads":
      output_file = "output/assembly/contigs.fa"
   elif seq_tech == "Solid":
      output_file = "output/assembly/nt_contigs.fa"
   elif seq_tech == "S_Paired_End_Reads" or seq_tech == "S_Mate_Paired_Reads":
      output_file = "output/assembly/nt_scaffolds.fa"
   
   # Prepare program
   progname = 'Assembler_denovo'
   prog = Program(path='wrapper_denovo_2.0.py',
      name=progname, wait=True,
      args=[]
      )
   proglist.add2list(prog)
   
   # Set up assembly parameters
   tech_setup(prog, seq_tech, files, args.trim_reads)
   # prog.append_args(['--n', '1'])
   
   # Execute program
   prog.execute()
   
   # THE SUCCESS OF THE ASSEMBLER IS VALIDATED
   status = prog.get_status()
   if status != 'Done':
      debug.graceful_exit("Error: Execution of the program failed!\n")
   
   # Copying assembler output to contig file path
   contigs_filename = '%s/contigs.fsa'%(os.getcwd())
   copy_file(output_file, contigs_filename)
   
   if os.path.exists(contigs_filename):
      # Sumarise Results
      results = { 'contigs_file': os.path.abspath(contigs_filename) }
      # Print json to stdout
      sys.stdout.write('%s\n'%json.dumps(results))
   else:
      debug.graceful_exit("Error: The Assembler was unable to produce contigs!"
                          "\n")
   
   # LOG THE TIMERS
   proglist.print_timers() 

def check_correct_number_of_files(file_count, seq_tech):
   ''' Checks wether or not the correct number of files have been submitted '''
   if file_count > 0:
      debug.log( "INPUT TYPE ::\t%s"%(seq_tech))
      if seq_tech in ["Paired_End_Reads", "Solid"] and file_count != 2:
         debug.graceful_exit(("Input Error: Exactly 2 files are required, "
                              "but %d files were submitted!")%(file_count))
      elif (seq_tech in ["S_Mate_Paired_Reads", "S_Paired_End_Reads"]
            and file_count != 4):
         debug.graceful_exit(("Input Error: Exactly 4 files are required, "
                              "but %d files were submitted!")%(file_count))
      elif seq_tech in ["Illumina", "454"] and file_count > 1:
         debug.graceful_exit(("Input Error: Exactly 1 file is required, but %d "
                              "files were submitted!")%(file_count))
   else:
      debug.graceful_exit("Input Error: No files were uploaded!")

def set_illumina_cmd(prog, seq_tech, files, trim_reads=False):
   ''' This function will set the command for the illumina technology '''
   debug.log("Setting Illumina Command...")
   # Adding Arguments to the assembly command
   prog.append_args(['velvet'])
   if seq_tech == "Illumina": prog.append_args(['--short', 'fastq', files[0]])
   elif seq_tech == "Paired_End_Reads":
      prog.append_args(['--shortPaired', 'fastq', files[0], files[1]])
      if trim_reads: prog.append_args([" --trim"])
   prog.append_args(['--add_velvetg', '-very_clean yes', '--sample',
                     'output']) # , '--wait'

def set_solid_cmd(prog, seq_tech, files, trim_reads=False):
   ''' This function will set the command for the SOLiD technology '''
   debug.log("Setting Solid Command...")
   # Adding Arguments to the assembly command
   prog.append_args(['solid'])
   
   r3 = r3q = f3 = f3q = f5 = f5q = "NA"
   f3_found = f3q_found = r3_found = r3q_found = f5q_found = f5_found = False
   genome_size = "5000000"
   
   if seq_tech=="Solid": # SINGLE END SOLID
      if len(files) < 2:
         debug.graceful_exit("Input Error: At least 2 files are needed to SOLiD"
                             " SE assembly\n")
      # now I have look for the quality ".qual" file and for the read ".csfasta"
      for index, el in enumerate(files):
         tmp_type = identify_solid_file_type(files[index])
         debug.log("FILE INDEX AND TYPE:\t"+ str(index) +" "+ tmp_type) 
         if tmp_type == "F3":
            if f3_found:
               debug.graceful_exit("Error: More than one SOLiD read of type F3"
                                   " uploaded\n")
            f3_found = True
            f3 = files[index]
         elif tmp_type == "F3q":
            if f3q_found:
               debug.graceful_exit("Error: More than one quality file of type"
                                   " F3 uploaded\n")
            f3q_found = True
            f3q = files[index]
      if f3 == "NA" or f3q == "NA":
         debug.graceful_exit("Error: quality file or read file not found\n")
      
      # Adding Arguments to the assembly command
      prog.append_args(['--se', f3, f3q])
   
   elif seq_tech=="S_Paired_End_Reads": # PAIRED END SOLID
      if len(files) < 4:
         debug.graceful_exit("Error: less than four files uploaded\n")
      # now I have look for the quality ".qual" file and for the read ".csfasta"
      for index, el in enumerate(files):
         tmp_type = identify_solid_file_type(files[index])
         debug.log("FILE INDEX AND TYPE:\t%s %s"%(index, tmp_type))
         if tmp_type == "F3":
            if f3_found:
               debug.graceful_exit("Error: more than one SOLiD read of type F3"
                                   " uploaded\n")
            f3_found = True
            f3 = files[index]
         elif tmp_type == "F3q":
            if f3q_found:
               debug.graceful_exit("Error: more than one quality file of type "
                                   "F3 uploaded\n")
            f3q_found = True
            f3q = files[index]
         elif tmp_type == "F5":
            if f5_found:
               debug.graceful_exit(("Error: more than one SOLiD read of type %s"
                                    " uploaded\n")%(tmp_type))
            f5_found = True
            f5 = files[index]
         elif tmp_type == "F5q":
            if f5q_found:
               debug.graceful_exit(("Error: more than one SOLiD quality file of"
                                    " type %s uploaded\n")%(tmp_type))
            f5q_found = True
            f5q = files[index]
      if f3 == "NA" or f3q == "NA" or f5 == "NA" or f5q == "NA":
         debug.graceful_exit("Error: quality file or read file not found\n")
      
      # Adding Arguments to the assembly command
      prog.append_args(['--pe', f3, f3q, f5, f5q])
   
   elif seq_tech=="S_Mate_Paired_Reads": # MATE PAIRED SOLID
      if len(files) < 4:
         debug.graceful_exit("Error: less than four files uploaded\n")
      # now I have look for the quality ".qual" file and for the read ".csfasta"
      for index, el in enumerate(files):
         tmp_type = identify_solid_file_type(files[index])
         debug.log("FILE INDEX AND TYPE:\t%s %s"%(index, tmp_type))
         if tmp_type == "F3":
            if f3_found:
               debug.graceful_exit("Error: more than one SOLiD read of type F3"
                                   " uploaded\n")
            f3_found = True
            f3 = files[index]
         elif tmp_type == "F3q":
            if f3q_found:
               debug.graceful_exit("Error: more than one quality file of type "
                                   "F3 uploaded\n")
            f3q_found = True
            f3q = files[index]
         elif tmp_type == "R3":
            if r3_found:
               debug.graceful_exit(("Error: more than one SOLiD read of type %s"
                                    " uploaded\n")%(tmp_type))
            r3_found = True
            r3 = files[index]
         elif tmp_type == "R3q":
            if r3q_found:
               debug.graceful_exit(("Error: more than one SOLiD quality file of"
                                    " type %s uploaded\n")%(tmp_type))
            r3q_found = True
            r3q = files[index]
      if f3 == "NA" or f3q == "NA" or r3 == "NA" or r3q == "NA":
         debug.graceful_exit("Error: quality file or read file not found\n")
      prog.append_args(['--mp', f3, f3q, r3, r3q])
   
   # Adding Arguments to the assembly command
   prog.append_args(['--rf', genome_size, '--sample', 'output',
                     '--add_solid', '\\"-NO_CORRECTION -NO_ANALYSIS\\"']) # , '--wait'

def identify_solid_file_type(fil):
   '''  '''
   debug.log("identifyFileType::START\nINPUT FILE: %s"%(fil))
   if not os.path.isfile(fil):
      debug.graceful_exit("Error: %s is not a valid file\n"%(fil))
   f_type = "NA" # it can f3q, f3, f5q, f5, r3q, r3
   tmp_type = ""
   # now we can open and nalyse the file
   with open(fil, "r") as fd:
      # we expect the files to have an ">" like fasta files and have a specific
      # two-characters at the and of the header, as follows
      #>...F3 -> F3 (SE) ".csfasta" or ".qual" file
      #>...F5 -> F5 (PE) ".csfasta" or ".qual" file
      #>...R3 -> R3 (Mate pairs) ".csfasta" or ".qual" file 
      #let's now analyse the file
      for index, line in enumerate(fd):
         if line[0] == ">": # then it is the header
            splitted = line.split("_")
            tmp_type = splitted[len(splitted)-1].strip() # get the type of file
         else: # then it is a line of the read or the quality file
            # if there are white-spaces it is a quality file otherwise it is a
            # read
            splitted = line.split(" ")
            if len(splitted) > 1: # then it is a quality file
               f_type = tmp_type + "q"
            else:
               f_type = tmp_type
         if f_type != "NA": # then we are done and can exit the for loop
            break
   
   debug.log("f_type :: %s\nidentifyFileType::END"%(f_type))
   return f_type

def set_454_cmd(prog, seq_tech, files, trim_reads=False):
   ''' This function will set the command for the 454 technology '''
   debug.log("Setting 454 Command...")
   # Adding Arguments to the assembly command
   prog.append_args(['newbler', '--datatype', '454'])
   
   if seq_tech=="454":
      # Adding Arguments to the assembly command
      prog.append_args(['--se', files[0]])
   
   elif seq_tech=="454_Paired_End_Reads":
      #check the input files format and decide the order
      files = ' '.join([x for x in files
                        if x.replace('.gz', '').split('.')[-1] in [
                           'fasta','fna','fa','fsa','fq','fastq','sff']
                        ])
      if files == '': # ERROR CHECK
         debug.graceful_exit('Error: no compatible file uploaded\n')
      
      # Adding Arguments to the assembly command
      prog.append_args(['--pe', files])
   
   # Adding Arguments to the assembly command
   prog.append_args(['--sample', 'output']) #, '--wait'

def set_ion_cmd(prog, seq_tech, files, trim_reads=False):
   ''' This function will set the command for the Ion-Torrent technology '''
   debug.log("Setting Ion Command...")
   
   # Adding Arguments to the assembly command
   prog.append_args(['newbler', '--datatype', 'IonTorrent'])
   
   files = ' '.join([x for x in files
                     if x.replace('.gz', '').split('.')[-1] in [
                        'fq', 'fastq']
                     ])
   if files == "":
      debug.graceful_exit("Error: no compatible file uploaded\n")
   
   # Adding Arguments to the assembly command
   prog.append_args(['--se', files, '--sample', 'output']) # , '--wait'

if __name__ == '__main__':
   main()
