#!/usr/bin/env python3
'''
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
'''
import sys, os, json
from cgecore import (check_file_type, debug, get_arguments, proglist, Program,
                     adv_dict, open_, mkpath)

# SET GLOBAL VARIABLES
service, version = "MLST", "2.0"
   
def main():
   ''' MAIN '''
   # PARSE ARGUMENTS
   # Add service specific arguments using the following format:
   #  (OPTION, VARIABLE,  DEFAULT,  HELP)
   args = get_arguments([
      ('-f', 'contigs', None, 'The input file (fasta file required)'),
      ('-o', 'organism', None, ('Scientific name of the organism in the sample '
                                 '(used to predict the MLST scheme)')),
      ('-d', 'db_dir', os.environ.get('CGEPIPELINE_DB', '/databases')+'/mlst', 'Path to database directory'),
      ('-s', 'mlst_scheme', None, 'The MLST scheme')
      ])
   
   # VALIDATE REQUIRED ARGUMENTS
   if args.db_dir is None:
      debug.graceful_exit("Input Error: No database directory was provided!\n")
   elif not os.path.exists(args.db_dir):
      debug.graceful_exit("Input Error: The specified database directory does not"
                          " exist!\n")
   else:
      # Check existence of config file
      db_config_file = '%s/config'%(args.db_dir)
      if not os.path.exists(db_config_file):
         debug.graceful_exit("Input Error: The database config file could not be "
                             "found!\n{}".format(db_config_file))

   infile = args.contigs
   if infile is None:
      debug.graceful_exit("Input Error: No Input file was provided!\n")
   # Check if 2 fastq files were provided
   if "," in infile:
      infiles = infile.split(",")
      try:
          infile = [os.path.abspath(infiles[0]), os.path.abspath(infiles[1])]
      except OsError as error:
          debug.graceful_exit("Input Error: Input file does not exist!\n{}\n".format(error))
      if check_file_type(infiles[0]) != 'fastq' or check_file_type(infiles[1]) != 'fastq':
         debug.graceful_exit("Input Error: When inputting more than on input files"
                             " they must be in fastq format\n")
      elif len(infiles) != 2: 
         debug.graceful_exit("Input Error: Only 2 (forward and reverse) fastq input"
                             " files are allowed\n")
      method = "kma"
      paired_end = True
   # Check if infile exists
   elif not os.path.exists(infile):
      debug.graceful_exit("Input Error: Input file does not exist!\n")
   # Check file format
   elif check_file_type(infile) == 'fastq':
      method = "kma"
      paired_end = False
      infile = os.path.abspath(infile)
   elif check_file_type(infile) == 'fasta':
      method = "blastn"
      infile = os.path.abspath(infile)   
   else:
      debug.graceful_exit("Input Error: Input file must be in fastq or fastq format.\n") 

   if args.mlst_scheme is None and args.organism is None:
      debug.graceful_exit("Input Error: Neither MLST scheme nor organism were "
                          "provided!\n")
   else:
      dbs = adv_dict({})
      extensions = []
      with open_(db_config_file) as f:
         for l in f:
            l = l.strip()
            if l == '': continue
            if l[0] == '#':
               #if 'extensions:' in l:
               #   extensions = [s.strip() for s in l.split('extensions:')[-1].split(',')]
               continue
            tmp = l.split('\t')
            if len(tmp) != 3:
               debug.graceful_exit(("Input Error: Invalid line in the database"
                                    " config file!\nA proper entry requires 3 tab "
                                    "separated columns!\n%s")%(l))
            db_prefix = tmp[0]
            name = tmp[1].split('#')[0]
            description = tmp[2]
            # Check if all db files are present
            #for ext in extensions:
            #   db_path = "%s/%s.%s"%(args.db_dir, db_prefix, ext)
            #   if not os.path.exists(db_path):
            #       debug.graceful_exit(("Input Error: The database file (%s) "
            #                            "could not be found!")%(db_path))
            dbs[db_prefix] = name

      if len(dbs) == 0:
         debug.graceful_exit("Input Error: No databases were found in the "
                             "database config file!")
      if args.mlst_scheme is not None:
         # Handle multiple MLST schemes
         args.mlst_scheme = args.mlst_scheme.split(',')
         # Check that the MLST scheme is valid
         databases = []
         for db_prefix in args.mlst_scheme:
            if db_prefix in dbs:
               databases.append(db_prefix)
            else:
               debug.graceful_exit("Input Error: Provided MLST scheme was not "
                                   "recognised! (%s)\n"%db_prefix)
      else:
         # Predict MLST scheme from args.organism input
         # Flip MLST scheme dict
         dbs = dbs.invert()
         # Add Manual Species Exceptions
         dbs['Shigella spp.'] = dbs['Escherichia coli']
         # Predict MLST Scheme from organism
         genus_lvl_name = args.organism.split()[0] + ' spp.'
         databases = dbs.get(args.organism.strip(), dbs.get(genus_lvl_name, None))
         if databases is None:
            debug.graceful_exit(
               ('No appropriate MLST scheme could be found for the provided '
                'organism! Following schemes were tried: %s and %s'
                )%(args.organism, genus_lvl_name))
   
   # Execute MLST for each scheme
   for db_prefix in databases:
      progname = 'MLST_%s'%db_prefix
      mkpath(progname) # Create service specific directory
      if method == "kma":
         #TODO index kma database on the fly
         if not os.path.exists("{0}/{1}/{1}.b".format(args.db_dir,db_prefix)):
            os.system("kma_index -i {0}/{1}/{1}.fsa -o {0}/{1}/{1}".format(args.db_dir,db_prefix))
         if paired_end == True: 
             infile_args = ['-i', infile[0], infile[1]]
         else:
             infile_args = ['-i', infile]
      else: 
         infile_args = ['-i', infile]
      prog = Program(path='mlst.py',
         name=progname, wdir=progname,
         args= infile_args + ['-p', args.db_dir,
               '-s', db_prefix,
               '-mp', method
               ]
         )

      prog.execute()
      proglist.add2list(prog)
   
   # Wait for and extract results
   results = {
      'runs': []
   }
   for progname in proglist.list:
      prog = proglist[progname]
      prog.wait(interval=30)
      # THE SUCCESS OF THE PROGRAMS ARE VALIDATED
      status = prog.get_status()

      results_file = '%s/data.json'%progname
      if status == 'Done' and os.path.exists(results_file):
         result = {
            'scheme_name':   '',
            'sequence_type': '',
            'genes':         []
            }
         with open(results_file, 'r') as json_data:
            d = json.load(json_data)

            result['scheme_name'] = d['mlst']['user_input']['organism']
            result['sequence_type'] = d['mlst']['results']['sequence_type']
            result['genes'] = []
            for locus, locus_d in d['mlst']['results']['allele_profile'].items():
               if locus_d.get('allele_name') == 'No hit found':
                  result['genes'].append(locus + ':No_hit_found')
               else:
                  result['genes'].append(locus_d.get('allele_name'))
            
         results['runs'].append(result)
   if len(results['runs']) == 0:
      debug.graceful_exit('Error: No results were found!\n')
   else:
      sys.stdout.write('%s\n'%json.dumps(results))
   
   # LOG THE TIMERS
   proglist.print_timers()

if __name__ == '__main__':
   main()
