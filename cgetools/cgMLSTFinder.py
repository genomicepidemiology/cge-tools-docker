#!/usr/bin/env python3
'''
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
'''
import sys, os, json
from cgecore import (check_file_type, debug, get_arguments, proglist, Program,
                     open_, adv_dict)

def main():
   ''' MAIN '''
   # PARSE ARGUMENTS
   # Add service specific arguments using the following format:
   #  (OPTION, VARIABLE,  DEFAULT,  HELP)
   args = get_arguments([
      ('-f', 'raw_reads', None, 'The input file (fastq file required)'),
      ('-f2', 'raw_reads2', None, 'The paired end file (fastq file only)'),
      ('-d', 'db', os.environ.get('CGEPIPELINE_DB', '/databases')+'/cgmlstfinder', 'Path to databases'),
      ('-s', 'species_scheme', None, 'The name of the cgMLST subdirectory. EG. yersinia_cgMLST'),
      ])
   
   # VALIDATE REQUIRED ARGUMENTS
   if args.db is None:
      debug.graceful_exit("Input Error: No database directory was provided!\n")
   elif not os.path.exists(args.db):
      debug.graceful_exit("Input Error: The specified database directory does "
                          "not exist!\n")
   
   if args.raw_reads is None:
      debug.graceful_exit("Input Error: No input was provided!\n")
   elif not os.path.exists(args.raw_reads):
      debug.graceful_exit("Input Error: Input file does not exist!\n")
   elif check_file_type(args.raw_reads) != 'fastq':
      debug.graceful_exit(('Input Error: Invalid raw reads format (%s)!\nOnly the '
                           'fastq format is recognised as a proper raw read format.'
                           '\n')%(check_file_type(args.raw_reads)))
   else:
       args.raw_reads = os.path.abspath(args.raw_reads)
   
   if args.raw_reads2 is not None:
      if not os.path.exists(args.raw_reads2):
         debug.graceful_exit("Input Error: Input file 2 does not exist!\n")
      elif check_file_type(args.raw_reads2) != 'fastq':
         debug.graceful_exit(('Input Error: Invalid raw reads 2 format (%s)!\nOnly the '
                              'fastq format is recognised as a proper raw read format.'
                              '\n')%(check_file_type(args.raw_reads2)))
      else:
         args.raw_reads2 = os.path.abspath(args.raw_reads2)
   
   # Execute Program
   progname = 'cgmlst'
   prog = Program(path='cgMLST.py',
      name=progname,
      args=['-db', args.db,
            '-o', 'results',
            '-t', 'tmp',
            '-s', args.species_scheme,
            args.raw_reads
            ]
      )
   if args.raw_reads2:
      prog.append_args([args.raw_reads2])
   prog.execute()
   proglist.add2list(prog)
   prog.wait(interval=25)
   
   # THE SUCCESS OF THE PROGRAMS ARE VALIDATED
   results = {
      'type':       'Unknown',
      'similarity': 'Unknown'
   }
   status = prog.get_status()
   if status == 'Done' and os.path.exists('results-st.txt'): #results.txt
      # Extract service results
      with open('results-st.txt', 'r') as f:
         # OUTPUT EXAMPLE tab
         # Sample_Names    cgST_Assigned   No_of_Allels_Found      Similarity
         # HMIK4389_R1_001 15574   1266    94.270000
         for l in f:
            if not l.strip(): continue
            if l.startswith('Sample'): continue
            tmp = l.split('\t')
            results['type'] = tmp[1].strip()
            results['similarity'] = tmp[3].strip()
            break
      
      sys.stdout.write('%s\n'%json.dumps(results))
   else:   
      sys.stderr.write('OBS: No results were found!\n')
   
   # LOG THE TIMERS
   proglist.print_timers()

if __name__ == '__main__':
   main()
