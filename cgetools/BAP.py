#!/usr/bin/env python3
''' CGE Pipeline '''
import sys, os
from cgecore import (check_file_type, debug, get_arguments, open_, proglist,
                     Program, load_json)

# SET GLOBAL VARIABLES
service, version = "CGEpipeline", "1.1"

def main():
   ''' MAIN '''
   # PARSE ARGUMENTS
   # Add service specific arguments using the following format:
   #(OPTION,   VARIABLE,  DEFAULT,  HELP)
   args = get_arguments([
   ('--wdir', 'workdir', None, 'The working directory'),
   ('--dbdir', 'dbdir', None, 'Overwrite default database directory'),
   ('--fq1',  'fastq1',  None, 'The forward fastq file'),
   ('--fq2',  'fastq2',  None, 'The reverse fastq file'),
   ('--fa',   'fasta',   None, ('The fasta file containing an assembled or draft'
                                ' genome')),
   ('--services',
      'services',
      'KmerFinder,PlasmidFinder,MLST,ResFinder,VirulenceFinder,SalmonellaTypeFinder,cgMLSTFinder',
      'The services to be executed'),
   ('--At', 'trim_reads', False, 'Trim fastq reads'),
   ('--Asp', 'sequencing_platform', None, 'Sequencing platform'),
   ('--Ast', 'sequencing_type', None, 'Sequencing type'),
   ('--KFd', 'kf_database', 'bacteria', 'KmerFinder database'),
   ('--Ms', 'mlst_scheme', None, 'MLST scheme'),
   ('--Ra', 'rf_database', None, 'ResFinder database'),
   ('--Rk',  'rf_threshold', '90.00', 'ResFinder threshold'),
   ('--Rl',  'rf_minlength',  '0.60', 'ResFinder minlength'),
   ('--VFk', 'vf_threshold', '90.00', 'VirulenceFinder threshold'),
   ('--PFd', 'pf_threshold', '0.80', 'PlasmidFinder threshold')
   ])
   
   # VALIDATION AND HANDLING OF ARGUMENTS
   if args.workdir is not None:
      os.chdir(args.workdir)
   contigs = args.fasta
   if contigs is not None:
      if check_file_type(contigs) != 'fasta':
         debug.graceful_exit("Error: Uploaded fasta file is not in fasta format!"
                             " Please provide a proper fasta file.\n")
      else:
         contigs = os.path.abspath(contigs)
   elif args.fastq1 is not None:
      if args.sequencing_platform is None:
         debug.graceful_exit("Error: Please provide a sequencing platform.\n")
      if args.sequencing_type is None:
         debug.graceful_exit("Error: Please provide a sequencing type.\n")
      if check_file_type(args.fastq1) != 'fastq':
         debug.graceful_exit("Error: Uploaded fastq file 1 is not in fastq "
                             "format! Please provide a proper fastq file.\n")
      if args.fastq1[-3:] == '.gz':
         # Gunzip file
         new_file = os.path.basename(args.fastq1)[:-3]
         with open_(new_file, 'wb') as f, open_(args.fastq1, 'rb') as gz:
            f.writelines(gz)
         fastqs = [os.path.abspath(new_file)]
      else:
         fastqs = [os.path.abspath(args.fastq1)]
      if args.fastq2 is not None:
         if check_file_type(args.fastq2) != 'fastq':
            debug.graceful_exit("Error: Uploaded fastq file 2 is not in fastq "
                                "format! Please provide a proper fastq file.\n")
         if args.fastq2[-3:] == '.gz':
            # Gunzip file
            new_file = os.path.basename(args.fastq2)[:-3]
            with open_(new_file, 'wb') as f, open_(args.fastq2, 'rb') as gz:
               f.writelines(gz)
            fastqs.append(os.path.abspath(new_file))
         else:
            fastqs.append(os.path.abspath(args.fastq2))
   else:
      debug.graceful_exit("Error: Input missing! Please provide a fasta or fastq "
                          "file as input.\n")
   if args.dbdir is not None:
      try: os.symlink(args.dbdir, '/databases')
      except FileExistsError as e:
         debug.graceful_exit("Error: /databases directory exists! Try removing "
                             "the --dbdir argument.\n")
   # HANDLE SERVICE ARGUMENT
   if args.services is None or args.services == '':
      debug.graceful_exit("Error: No services requested!\n")
   else:
      services = args.services.split(',')
      debug.log("Requested Services:\n%s"%('\n'.join(services)))
   
   if (     'ResFinder' in services
      and
            (    args.rf_database == ''
            or args.rf_minlength == ''
            or args.rf_threshold == ''
            )
      ):
      debug.graceful_exit("Error: Missing ResFinder threshold or database or "
                        "minlength arguments!\n")
   
   if 'PlasmidFinder' in services and args.pf_threshold == '':
      debug.graceful_exit("Error: PlasmidFinder threshold was missing!\n")
   
   if 'VirulenceFinder' in services and args.vf_threshold == '':
      debug.graceful_exit("Error: Missing VirulenceFinder threshold arguments!\n")

   # EXECUTE CONTIGS INDEPENDENT PIPELINE SERVICES
   if contigs is None:
      # ASSEMBLE THE READS
      progname='Assembler'
      prog = Program(path='Assembler',
         name=progname, wdir=progname,
         args=['--files', ','.join(fastqs),
               '--sequencing_platform', args.sequencing_platform,
               '--sequencing_type', args.sequencing_type
               ]
         )
      if args.trim_reads: prog.append_args(['--trim'])
      prog.execute()
      proglist.add2list(prog)
   
   # SPECIES TYPING
   species = 'Unknown'
   if 'KmerFinder' in services:
      progname = 'KmerFinder'
      prog = Program(path='KmerFinder',
         name=progname, wdir=progname,
         args=['-s', args.kf_database
               ]
         )
      if contigs is None:
         prog.append_args(['-f', fastqs[0]])
      else:
         prog.append_args(['-f', contigs])
      prog.execute()
      proglist.add2list(prog)
   
   # EXECUTE CONTIGS DEPENDENT PIPELINE SERVICES
   if contigs is None:
#      Checking the status of the assembler, and waiting for it to finish if it is still running.
      # Verify that assembly is submitted
      if proglist.exists('Assembler'):
         prog = proglist['Assembler']
         status = prog.get_status()
         if status == 'Executing':
            prog.wait()
            status = prog.get_status()
            # THE SUCCESS OF THE ASSEMBLY IS VALIDATED
            if status == 'Done':
               debug.log('The Assembly was finished...')
               # Retrieve contigs path
               result = {}
               fp = prog.stdout
               if prog.wdir != '':
                  fp = '%s/%s'%(prog.wdir, fp)
               if not os.path.exists(fp):
                  debug.log("Warning: Result file %s, could not be found!"%fp)
               elif os.path.getsize(fp) == 0:
                  debug.log("Warning: Result file %s, was empty!"%fp)
               else:
                  debug.log("Getting results from %s..."%fp)
                  result = load_json(fp)
               contigs = result.get('contigs_file', None)
               debug.log("contigs %s %s..."%(contigs, check_file_type(contigs)))
               if (contigs is None
                  or not os.path.exists(contigs)
                  or check_file_type(contigs) != 'fasta'):
                  debug.log('No suitable contigs was created by the assembler!')
                  status = 'Failure'
         if status == 'Done': assembly_done = True
         else: assembly_done = False
      else: assembly_done = True
   else: assembly_done = True
   if not assembly_done:
      debug.log("\nError: Assembly failed, thus all contigs dependent services "
               "were not able to run!")
   else:
      progname='ContigAnalyzer'
      prog = Program(path='ContigAnalyzer',
         name=progname, wdir=progname,
         args=['-f', contigs]
         )
      prog.execute()
      proglist.add2list(prog)
      
      if 'ResFinder' in services:
         progname='ResFinder'
         prog = Program(path='ResFinder',
            name=progname, wdir=progname,
            args=['-k', args.rf_threshold,
                  '-l', args.rf_minlength,
                  '-f', contigs
                  ]
            )
         if args.rf_database is not None:
            prog.append_args(['-s', args.rf_database])
         prog.execute()
         proglist.add2list(prog)
      # Wait on Species Prediction
      if len(services) > 0:
         species = 'Unknown'
         lineage = []
         prog = proglist['KmerFinder']
         prog.wait()
         status = prog.get_status()
         # THE SUCCESS OF SPECIES IDENTIFICATION IS VALIDATED
         debug.log('KmerFinder status: %s'%status)
         if status == 'Done':
            result = None
            if isinstance(prog.stdout, str):
               fp = prog.stdout
               if prog.wdir != '':
                  fp = '%s/%s'%(prog.wdir, fp)
               if not os.path.exists(fp):
                  debug.log("Warning: Result file %s, could not be found!"%fp)
               elif os.path.getsize(fp) == 0:
                  debug.log("Warning: Result file %s, was empty!"%fp)
               else:
                  debug.log('Getting results from %s'%fp)
                  result = load_json(fp)
            else:
               result = load_json(prog.stdout)
            if result is not None:
               species = result.get('species', 'Unknown')
               lineage = result.get('lineage', [])
            else:
               species = 'Unknown'
               lineage = []
            if lineage == ['Unknown']: lineage=[]
         #debug.log('\nSpecies: %s\nLineage: %s'%(species, ';'.join(lineage)))
         debug.log('\nSpecies: %s\nLineage: %s'%(species, lineage))
         
         # SEQUENCE TYPING
         if 'MLST' in services:
            # Find MLST Scheme
            mlst_scheme = None
            if species.lower() == 'unknown' or species == '':
               debug.log('MLST could not be executed since the species was not '
                        'predicted!')
            else:
               progname='MLST'
               prog = Program(path='MLST',
                  name=progname, wdir=progname,
                  args=['-f', contigs]
                  )
               if mlst_scheme is not None:
                  prog.append_args(['-s', mlst_scheme])
               else:
                  prog.append_args(['-o', species])
               prog.execute()
               proglist.add2list(prog)
         
         # PLASMID TYPING (includes pMLST)
         if 'PlasmidFinder' in services:
            config = '/databases/plasmidfinder/config'
            if not os.path.exists(config):
               debug.graceful_exit("Setup Error: The plasmid database config "
                                   "file could not be found!")
            
            databases = {}
            with open(config, 'r') as f:
               for l in f:
                  l = l.strip()
                  if not l: continue
                  if l.startswith("#"): continue
                  tmp = l.split("\t")
                  for x in tmp[2].split(':',1)[-1].strip().split(','):
                     if not x in databases:
                        databases[x] = []
                     databases[x].append(tmp[0])
            
            if len(lineage) > 0:
               db_selection = ','.join([','.join(databases[db]) for db in databases.keys() if db in lineage])
            else:
               db_selection = ','.join(set([db for dbs in databases.values() for db in dbs]))
            debug.log('\nLiniage check: %s in %s'%(databases.keys(), lineage))
            if db_selection != '':
               progname='PlasmidFinder'
               prog = Program(path='PlasmidFinder',
                  name=progname, wdir=progname,
                  args=['-k', args.pf_threshold,
                        '-s', db_selection,
                        '-f', contigs
                        ]
                  )
               prog.execute()
               proglist.add2list(prog)
         
         # Virus phenotyping
         if 'VirulenceFinder' in services:
            databases = {
               'Escherichia coli': ['virulence_ecoli'],
               'Shigella': ['virulence_ecoli']
            }
            if len(lineage) > 0:
               db_selection = ','.join([','.join(databases[db]) for db in databases.keys() if db in lineage])
            else:
               db_selection = ','.join(set([db for dbs in databases.values() for db in dbs]))
            debug.log('Liniage check: %s in %s'%(databases.keys(), lineage))
            if db_selection != '':
               progname='VirulenceFinder'
               prog = Program(path='VirulenceFinder',
                  name=progname, wdir=progname,
                  args=['-k', args.vf_threshold,
                        '-s', db_selection,
                        '-f', contigs
                        ]
                  )
               prog.execute()
               proglist.add2list(prog)
         
         # Salmonella Typing
         if (    'SalmonellaTypeFinder' in services
             and args.fastq1 is not None
             and 'Salmonella' in lineage):
            progname='SalmonellaTypeFinder'
            prog = Program(path='SalmonellaTypeFinder',
               name=progname, wdir=progname,
               args=['-f', fastqs[0]]
               )
            if args.sequencing_type == 'paired' and len(fastqs) == 2:
               prog.append_args(['-f2', fastqs[1]])
            
            prog.execute()
            proglist.add2list(prog)
         
         # cgMLSTFinder
         debug.log('cgMLSTFinder check: %s %s %s'%('cgMLSTFinder' in services, args.fastq1 is not None, len(lineage) > 0))
         if 'cgMLSTFinder' in services and args.fastq1 is not None and len(lineage) > 0:
            databases = {
               'Escherichia coli': ['ecoli_cgMLST'],
               'Campylobacter':    ['campy_cgMLST'],
               'Listeria coli':    ['listeria_cgMLST'],
               'Yersinia':         ['yersinia_cgMLST'],
               'Salmonella':       ['salmonella_cgMLST_2']
            }
            if len(lineage) > 0:
               db_selection = ','.join([','.join(databases[db]) for db in databases.keys() if db in lineage])
            # else:
            #    db_selection = ','.join(set([db for dbs in databases.values() for db in dbs]))
            debug.log('Liniage check: %s in %s'%(databases.keys(), lineage))
            progname='cgMLSTFinder'
            prog = Program(path='cgMLSTFinder',
               name=progname, wdir=progname,
               args=['-f', fastqs[0],
                     '-s', db_selection
                     ]
               )
            if args.sequencing_type == 'paired' and len(fastqs) == 2:
               prog.append_args(['-f2', fastqs[1]])
            
            prog.execute()
            proglist.add2list(prog)
   
   # Extract results
   debug.log('program list: %s\n'%proglist.list)
   debug.log_no_newline('\nWait on all services to finish and extracting results...')
   results = {}
   for progname in proglist.list:
      result = {}
      prog = proglist[progname]
      prog.wait()
      if isinstance(prog.stdout, str):
         fp = prog.stdout
         if prog.wdir != '':
            fp = '%s/%s'%(prog.wdir, fp)
         if not os.path.exists(fp):
            debug.log("Warning: Result file %s, could not be found!"%fp)
         elif os.path.getsize(fp) == 0:
            debug.log("Warning: Result file %s, was empty!"%fp)
         else:
            debug.log('Getting results from %s'%fp)
            result = load_json(fp)
      else:
         result = load_json(prog.stdout)
      if progname == 'KmerFinder':
         results['species'] = result.get('species', 'NA')
      elif progname == 'Assembler':
         results['contigs_file'] = contigs if contigs is not None else 'NA'
      elif progname == 'ContigAnalyzer':
         results['sequencing_size'] = result.get('sequencing_size', 'NA')
         results['genome_size'] = result.get('sum', 'NA')
         results['contigs'] = result.get('length', 'NA')
         results['n50'] = result.get('n50', 'NA')
         results['depth'] = result.get('depth', 'NA')
      elif progname == 'MLST':
         results['mlst'] = []
         results['mlst_genes'] = []
         if 'runs' in result:
            for run in result['runs']:
               scheme = run.get('scheme_name', 'NA')
               st = run.get('sequence_type', 'NA')
               genes = run.get('genes', ['NA'])
               results['mlst'].append("%s[ST%s]"%(scheme, st))
               results['mlst_genes'].append("%s[%s]"%(scheme, ','.join(genes)))
         
         results['mlst'] = ','.join(results['mlst'])
         results['mlst_genes'] = ','.join(results['mlst_genes'])
         if len(results['mlst']) == 0:
            results['mlst'] = 'NA'
         if len(results['mlst_genes']) == 0:
            results['mlst_genes'] = 'NA'
      elif progname == 'ResFinder':
         results['resistance_genes'] = ','.join(result.get('genes', ['NA']))
      elif progname == 'VirulenceFinder':
         results['virulence_genes'] = ','.join(result.get('genes', ['NA']))
      elif progname == 'SalmonellaTypeFinder':
         results['salmonella_type'] = result.get('type', ['NA'])
      elif progname == 'cgMLSTFinder':
         results['cgst'] = "%s (%s)"%(result.get('type', 'NA'),
                                        result.get('similarity', 'NA'))
      elif progname == 'PlasmidFinder':
         results['plasmids'] = []
         plasmids = result.get('plasmids', None)
         if plasmids is not None:
            for p in plasmids:
               results['plasmids'].append(p['plasmid'])
            
            results['plasmids'] = ','.join(sorted(results['plasmids']))
         else:
            results['plasmids'] = 'NA'
         results['pmlsts'] = []
         pmlsts = result.get('pmlsts', None)
         if pmlsts is not None and len(pmlsts) > 0:
            for p in pmlsts:
               results['pmlsts'].append("%s[%s]"%(p['scheme_name'],
                                                p['sequence_type']))
            
            results['pmlsts'] = ','.join(results['pmlsts'])
         else:
            results['pmlsts'] = 'NA'
   
   # PROCESS RESULTS AND PRESENT RESULTS AS TAB-SEP FILE
   # 209605650	2997471	117	141273	69.93	Staphylococcus aureus	saureus[ST-239]	saureus[ARCC-2,AROE-3,GLPF-1,GMK_-1,PTA_-4,TPI_-4,YQIL-3]	NA	aac(6')-aph(2'')-like,ant(6)-Ia-like,aph(3')-III,blaZ,dfrG,erm(A),mecA-like,spc,tet(M)-like	NA	NA	NA	NA
   # 714535360	4904529	138	303261	145.69	Salmonella enterica	senterica[ST-19]	senterica[AROC-10,DNAN-7,HEMD-12,HISD-9,PURE-5,SUCA-9,THRA-2]	NA		NA	IncFIB(S),IncFII(S)		IncF[S1:A-:B17]
   headers = ['contigs_file','sequencing_size','genome_size','contigs','n50',
              'depth','species','mlst','mlst_genes','resistance_genes',
              'virulence_genes','plasmids','pmlsts','salmonella_type', 'cgst']
   with open('out.tsv','w') as out:
      out.write('%s\n%s\n'%(
         '\t'.join(headers),
         '\t'.join([str(results[k]) if k in results else 'NA' for k in headers])
         ))
   
   # TIME STAMPS / BENCHMARKING
   proglist.print_timers()

if __name__ == '__main__':
   main()
