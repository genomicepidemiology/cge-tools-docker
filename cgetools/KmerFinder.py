#!/usr/bin/env python3
'''
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
'''
import sys, os, json, pprint
from cgecore import (check_file_type, debug, get_arguments, proglist, Program, mkpath,
                     open_)

# SET GLOBAL VARIABLES
service, version = "KmerFinder", "3.0"

def main():
   ''' MAIN '''
   # PARSE ARGUMENTS
   # Add service specific arguments using the following format:
   #  (OPTION, VARIABLE,  DEFAULT,  HELP)
   args = get_arguments([
      ('-f', 'file', None, 'The input file (fasta file required)'),
      ('-d', 'db_dir', os.environ.get('CGEPIPELINE_DB', '/databases')+'/kmerfinder', 'Path to database directory'),
      ('-s', 'databases', 'bacteria', (
         'Premade KmerFinder database name:'
         'bacteria, plasmids, typestrains, '
         'fungi, protozoa, archaea'
         )),
      ('-p', 'prefix', '', ('DB Prefix only used for private databases')),
      ])
   
   # VALIDATE REQUIRED ARGUMENTS
   if args.db_dir is None:
      debug.graceful_exit("Input Error: No database directory was provided!\n")
   elif not os.path.exists(args.db_dir):
      debug.graceful_exit("Input Error: The specified database directory does not"
                        " exist!\n")
   else:
      # Check existence of config file
      db_config_file = '%s/config'%(args.db_dir)
      if not os.path.exists(db_config_file):
         debug.graceful_exit("Input Error: The database config file could not be "
                           "found!")
   
   if args.file is None:
      debug.graceful_exit("Input Error: No input file were provided!\n")
   elif not os.path.exists(args.file):
      debug.graceful_exit("Input Error: Input file does not exist!\n")
   elif not check_file_type(args.file) in ['fasta','fastq']:
      debug.graceful_exit(('Input Error: Invalid file format (%s)!\nOnly the fasta'
                           ' or fastq format is recognised as a proper file '
                           'format.\n')%(check_file_type(args.file)))
   
   if (args.databases is None or args.databases is ''):
      debug.graceful_exit("Input Error: No database was specified!\n")
   else:
      dbs = {}
      extensions = []
      with open_(db_config_file) as f:
         for l in f:
            l = l.strip()
            if l == '': continue
            if l[0] == '#':
               if 'extensions:' in l:
                  extensions = [s.strip() for s in l.split('extensions:')[-1].split(',')]
               continue
            tmp = l.split('\t')
            if len(tmp) != 3:
               debug.graceful_exit(("Input Error: Invalid line in the database"
                                    " config file!\nA proper entry requires 3 tab "
                                    "separated columns!\n%s")%(l))
            db_prefix = tmp[0]
            #name = tmp[1].lower().replace(' ', '_')
            name = tmp[0].split(".")[0]
            description = tmp[2]
            # Check if all db files are present
            for ext in extensions:
               db_path = "%s/%s.%s"%(args.db_dir, db_prefix, ext)
               if not os.path.exists(db_path):
                  debug.graceful_exit(("Input Error: The database file (%s) "
                                       "could not be found!")%(db_path))
            # Check existence of taxonomy file
            tax_path = "%s/%s.name"%(args.db_dir, db_prefix.split('.')[0])
            if not os.path.exists(tax_path):
               debug.log('Warning: Taxonomy file not found! (%s)'%(tax_path))
               tax_path = None
            kmer_prefix = ''
            if 'prefix=' in description:
               kmer_prefix = description.split('prefix=')[-1].strip()
            if name not in dbs: dbs[name] = []
            dbs[name].append((db_prefix, tax_path, kmer_prefix))
      
      if len(dbs) == 0:
         debug.graceful_exit("Input Error: No databases were found in the "
                             "database config file!")
      # Handle multiple databases
      args.databases = args.databases.split(',')
      # Check that the Databases are valid
      databases = []
      for db in args.databases:
         if db.lower() in dbs:
            # Predefined DB
            db = db.lower()
            databases.append((db, dbs[db][0][0], dbs[db][0][1], dbs[db][0][2]))
         elif os.path.exists(db):
            # Private DB
            databases.append(('private', db, None, args.prefix))
         else:
            debug.graceful_exit("Input Error: Provided database was not "
                                "recognised or found! (%s)\n"%db)
   
   
   # Execute script   
   for (db_name, db_prefix, tax_path, prefix) in databases:
      progname = 'KmerFinder_%s'%(db_name)
      # Create specific directory for results
      mkpath(progname)
      tf_result = '%s/results/'%(progname)
      prog = Program(path= 'kmerfinder.py',
         name=progname,
         args=['-db', "%s/%s"%(args.db_dir, db_prefix),
               '-i', args.file,
               '-o', tf_result
               ]
         )
      # Add conditional arguments
      if tax_path is not None:
         prog.append_args(['-tax', tax_path,
               '-x'])

      # Execute and wait on species prediction
      prog.execute()
      proglist.add2list(prog)
      prog.wait(interval=25)
      
      # THE SUCCESS OF THE PROGRAMS IS VALIDATED
      # lineage = None
      # predicted_species = None
      # taxid_species = None
      # closest_template = None
      # taxid_template = None
      # Kmer_hits = None
      # Kmers_template = None
      # Kmer_coverage = None
      # coverage_query = None
      # coverage_template = None
      status = proglist['KmerFinder_%s'%(db_name)].get_status()

      # Wait for and extract results
      results = {
       'runs': []
      }
      results_file = '%s/results/data.json'%progname
      results_file_txt = '%s/results/results.txt'%progname
      results_file_spa = '%s/results/results.spa'%progname
      if tax_path is None:
         debug.log('No taxonomy file was given!')
         if status == 'Done' and os.path.exists(results_file_spa):
            debug.log('Program finished successfully. Results can be found in the %s file'%(results_file_spa))
            with open(results_file, 'r') as json_data:
                   d = json.load(json_data)
            sys.stdout.write('%s\n'%json.dumps(d))
         if status not in ['Done','Success']:
            debug.graceful_exit("Error: Execution of the program failed!\n")  
      else:
         if status == 'Done' and os.path.exists(results_file):

            # find best hit
            with open(results_file_txt, 'r') as results_data:
               # Skip header
               _ = results_data.readline()
               hit = results_data.readline().split('\t') # First hit
               if len(hit)==1 and hit[0]=='':
                  debug.graceful_exit('Error: No results were found!\n')
               else:   
                  best_hit = hit[14]
            result = {
               'kmerfinder_database':   '',
               'lineage': '',
               'species':         [],
               'closest_template': '',
               'coverage_template': '',
               }
            try:
               debug.log('KmerFinder result_file: %s'%results_file)    
               with open(results_file, 'r') as json_data:
                   d = json.load(json_data)
                   result['kmerfinder_database'] = d['kmerfinder']['user_input']['database']
                   #result['lineage'] = d['kmerfinder']['results']['species_hits'][list(d['kmerfinder']['results']['species_hits'].keys())[0]]['Taxonomy'].split(';')[-4]
                   if d['kmerfinder']['results']['species_hits'][best_hit]['Taxonomy'].split(';')[0] == 'unknown':
                       result['lineage'] = 'unknown'
                   else:
                       result['lineage'] = d['kmerfinder']['results']['species_hits'][best_hit]['Taxonomy']
                   if d['kmerfinder']['results']['species_hits'][best_hit]['Taxonomy'].split(';')[0] == 'unknown':
                       result['species'] = 'unknown'
                   else:
                       result['species'] = d['kmerfinder']['results']['species_hits'][best_hit]['Species'].strip()
                   result['closest_template'] = list(d['kmerfinder']['results']['species_hits'].keys())[0]
                   result['coverage_template'] = d['kmerfinder']['results']['species_hits'][best_hit]['tot_coverage']
                   results['runs'].append(result)
            except Exception as e:
                debug.log('All or some Kmer Results could not be found!\n%s'%(e))
            else: status = 'Success'
         else: status = 'Failure'
         if status not in ['Done','Success']:
            debug.graceful_exit("Error: Execution of the program failed!\n")
         # Sumarise Results
         results = {
            'lineage':            result['lineage'],
            'species':            result['species'],
            'closest_template':   result['closest_template'],
            'coverage_template':  result['coverage_template']
            # 'taxid_species':      taxid_species,
            # 'taxid_template':     taxid_template,
            # 'kmers_hit':          Kmer_hits,
            # 'kmers_template':     Kmers_template,
            # 'kmer_coverage':      Kmer_coverage,
            # 'coverage_query':     coverage_query,
         }
         #debug.log('KmerFinder results: %s'%results)

         if len(results['lineage']) is None:
            debug.graceful_exit('Error: No results were found!\n')
         else:
            sys.stdout.write('%s\n'%json.dumps(results))
            #print("\n##~ KmerFinder Results : Top Hit ~##\n")
            #print("--------------------------------------\n")
            #for k,v in results.items():
            #   print(k,": ", v)
            #print("--------------------------------------\n")
            #pprint.pprint(results)
   # LOG THE TIMERS
   proglist.print_timers()

if __name__ == '__main__':
   main()

