#!/usr/bin/env python
from setuptools import setup

with open("README.md", 'r') as f:
    try:
        long_description = f.read()
    except UnicodeDecodeError:
        long_description = ""

with open("cgetools/__init__.py", 'r') as f:
    for l in f:
        if l.startswith('__version__'):
            version = l.split('=')[1].strip().strip('"')

setup(
    name="cgetools",
    packages=['cgetools'],
    version=version,
    description="The Center for Genomic Epidemiology Toolbox",
    long_description=long_description,
    license="Apache License, Version 2.0",
    entry_points={
        'console_scripts': [
            'Assembler = cgetools.Assembler:main',
            'ContigAnalyzer = cgetools.ContigAnalyzer:main',
            'KmerFinder = cgetools.KmerFinder:main',
            'ResFinder = cgetools.ResFinder:main',
            'MLST = cgetools.MLST:main',
            'PlasmidFinder = cgetools.PlasmidFinder:main',
            'pMLST = cgetools.pMLST:main',
            'VirulenceFinder = cgetools.VirulenceFinder:main',
            'SalmonellaTypeFinder = cgetools.SalmonellaTypeFinder:main',
            'cgMLSTFinder = cgetools.cgMLSTFinder:main',
            'BAP = cgetools.BAP:main'
            ]
        },
    test_suite="tests"
)
