#!/usr/bin/env bash

if [ -z "${1}" ]; then
   echo "please provide a path for where to store the databases"
   read path
else
   path=$1
fi

if  [ -d "$path" ]; then
   # Download database repositories
   #wget -P $path/kmerfinder ftp://ftp.cbs.dtu.dk/public//CGE/databases/KmerFinder/database/*
   git clone "https://bitbucket.org/genomicepidemiology/kmerfinder_db.git" $path/kmerfinder
   git clone "https://bitbucket.org/genomicepidemiology/mlst_db.git" $path/mlst
   git clone "https://bitbucket.org/genomicepidemiology/plasmidfinder_db.git" $path/plasmidfinder
   git clone "https://bitbucket.org/genomicepidemiology/pmlst_db.git" $path/pmlst
   git clone "https://bitbucket.org/genomicepidemiology/resfinder_db.git" $path/resfinder
   git clone "https://bitbucket.org/genomicepidemiology/virulencefinder_db.git" $path/virulencefinder
   git clone "https://bitbucket.org/genomicepidemiology/salmonellatypefinder_db.git" $path/salmonellatypefinder
   git clone "https://bitbucket.org/genomicepidemiology/cgmlstfinder_db.git" $path/cgmlstfinder
   #wget -P $path/cgmlstfinder ftp://ftp.cbs.dtu.dk/public/CGE/databases/cgMLSTFinder/*.tar.gz
   db_path=$(pwd)
   # Install KmerFinder Repository
   cd $path/kmerfinder/
   bash INSTALL.sh . all
   # Install cgMLSTFinder Databases
   cd $db_path
   cd $path/cgmlstfinder/
   bash INSTALL.sh . all
   cd $db_path
   #for f in *.tar.gz; do
   #   tar xzf $f && rm -f $f
   #done
else
   echo "Error: Path '$path' does not exist!"
   exit
fi

# Always exit with ok status
exit 0
