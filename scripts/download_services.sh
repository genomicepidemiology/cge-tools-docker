#!/usr/bin/env bash

if [ -d "services" ]; then
   echo $"services directory already exists!\nContinue installing services?"
   select yn in "Yes" "No"; do
      case $yn in
         Yes ) break;;
         No ) exit;;
      esac
   done
else
   mkdir services
fi

# Download services
git clone --branch 0.2.0 --depth 1 "https://bitbucket.org/genomicepidemiology/assembler.git" services/assembler
git clone --branch 3.0.0 --depth 1 "https://bitbucket.org/genomicepidemiology/kmerfinder.git" services/kmerfinder
git clone --branch 2.0.2 --depth 1 "https://bitbucket.org/genomicepidemiology/mlst.git" services/mlst
git clone --branch 1.1.1 --depth 1 "https://bitbucket.org/genomicepidemiology/plasmidfinder.git" services/plasmidfinder
git clone --branch 0.1.0 --depth 1 "https://bitbucket.org/genomicepidemiology/pmlst.git" services/pmlst
git clone --branch 2.3.1 --depth 1 "https://bitbucket.org/genomicepidemiology/resfinder.git" services/resfinder
git clone --branch 0.0.1 --depth 1 "https://bitbucket.org/genomicepidemiology/virulencefinder.git" services/virulencefinder
git clone --branch 0.1.0 --depth 1 "https://bitbucket.org/genomicepidemiology/salmonellatypefinder.git" services/salmonellatypefinder
git clone --branch 1.0.1 --depth 1 "https://bitbucket.org/genomicepidemiology/cgmlstfinder.git" services/cgmlstfinder

# Always exit with ok status
exit 0
