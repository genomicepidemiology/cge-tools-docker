CGE Tools
=========

This project documents the Tools and the Pipeline of the Center for Genomic Epidemiology (CGE) running in a Docker container

Authors: 
   Martin Christen F. Thomsen

Installation
============

Recommended installation instructions can be found [here](https://docs.docker.com/engine/installation/) which install Docker and the VM.


Test that Docker is installed properly:
```bash
docker version
docker run hello-world
```

Install The CGE Docker Toolkit:
```bash
# For MacOS running docker-machine, do the following to make sure the machine
# is running, and that the environment is set:
docker-machine stop default
docker-machine start default
eval "$(docker-machine env default)"

# If you are stuck on: Waiting for SSH to be available...
# You are probably still running Docker through docker-machine.
# To change from docker-machine setup to Docker for Mac, run the following
# command to unset your docker-machine environment
eval $(docker-machine env --unset)

# Clone this repository
git clone https://bitbucket.org/genomicepidemiology/cge-tools-docker
cd cge-tools-docker

# Download service repositories (must be done before building phase!)
scripts/download_services.sh services

# Build cgetools Docker image
docker build -t cgetools .

# Note: There are several steps to go through and it will take several minutes to build.
```

Install the databases
```bash
# Download and install databases
dbpath=/path/to/databases # don't use relative paths
mkdir $dbpath
scripts/download_databases.sh $dbpath

# Note: The KmerFinder database contains large files (several GB) which can take some time to download.
# (This process can be done in parallel with the build command above to save time.)
```

Test
====
```bash
# Test BAP contigs
docker run -ti --rm -w /workdir -v $(pwd):/workdir \
   cgetools BAP --dbdir /usr/src/cgepipeline/test/databases \
   --fa /usr/src/cgepipeline/test/test.fa

# Check the output
cat out.tsv

# The output should look exactly like so:
#contigs_file	sequencing_size	genome_size	contigs	n50	depth	species	mlst	mlst_genes	resistance_genes	virulence_genes	plasmids	pmlsts	salmonella_type	cgst
#NA	NA	4834953	606	28438	NA	Escherichia coli	Escherichia coli#1[ST44]	Escherichia coli#1[recA_7,fumC_11,icd_8,mdh_8,gyrB_4,purA_8,adk_10]	aac(3)-IIa,aac(6')Ib-cr,aadA5,strA,strB	gad	Col(MG828),Col(MGD2),IncFIA,IncFIB(AP001918),IncFII(Yp)	IncF[F31:A4:B1]	NA	NA

# Test BAP Illumina paired end reads
docker run -ti --rm -w /workdir -v $(pwd):/workdir \
   cgetools BAP --dbdir /usr/src/cgepipeline/test/databases \
   --fq1 /usr/src/cgepipeline/test/test_1.fq.gz \
   --fq2 /usr/src/cgepipeline/test/test_2.fq.gz --Asp Illumina --Ast paired

# Check the contigs and output
head -3 Assembler/contigs.fsa

#>NODE_1_length_720_cov_6.647222
#AGCTCACTGCATAGCTATGCATGAAAGTGAATGGCGATCGGTTTGGGGCCTTACGGCGTT
#CATACCGTCTGTTTTCGACAGTTTCTCTCCGGGAAGCTAATCTGCCATAAGCCTGGATAA

cat out.tsv

#contigs_file	sequencing_size	genome_size	contigs	n50	depth	species	mlst	mlst_genes	resistance_genes	virulence_genes	plasmids	pmlsts	salmonella_type	cgst
/workdir/Assembler/contigs.fsa	NA	16284	47	325	NA	Escherichia coli	Escherichia coli#1[STUnknown]	Escherichia coli#1[gyrB:No_hit_found,recA:No_hit_found,mdh:No_hit_found,purA:No_hit_found,fumC:No_hit_found,adk:No_hit_found,icd:No_hit_found]	NA	NA	Col(MG828),Col(MGD2)	NA	NA	NA (NA)


# Test downloaded databases in $dbpath
docker run -ti --rm -w /workdir \
   -v $dbpath:/databases \
   -v $(pwd):/workdir \
   cgetools BAP --fa /usr/src/cgepipeline/test/test.fa

# Check the output
cat out.tsv

#contigs_file	sequencing_size	genome_size	contigs	n50	depth	species	mlst	mlst_genes	resistance_genes	virulence_genes	plasmids	pmlsts	salmonella_type	cgmlst
#NA	NA	4834953	606	28438	NA	Escherichia coli	ecoli[ST44],ecoli_2[ST2]	ecoli[adk-10,fumc-11,gyrb-4,icd-8,mdh-8,pura-8,reca-7],ecoli_2[dinb_8,icda_2,pabb_7,polb_3,putp_7,trpa_1,trpb_4,uida_2]	aac(3)-IIa,aac(6')-Ib-cr,aac(6')Ib-cr,aadA5,aph(3'')-Ib,aph(6)-Id,blaCTX-M-15,blaOXA-1,catB4,dfrA17,mph(A),sul1,sul2,tet(B)	gad_27_U00096	Col(MG828),Col(MGD2),IncFIA,IncFIB(AP001918),IncFII(Yp)	IncF[F31:A4:B1]	NA	NA
```


Usage
=====
```bash
# Run terminal shell on selected image
docker run -t -i cgetools /bin/bash

# Assembly Illumina
docker run -ti --rm -w /workdir \
   -v $(pwd):/workdir \
   cgetools Assembler --sequencing_platform Illumina --sequencing_type paired \
   --files "/workdir/services/assembler/test/ipe_R1.fq.gz,/workdir/services/assembler/test/ipe_R2.fq.gz"

# Assembly LS454
docker run -ti --rm -w /workdir \
   -v $(pwd):/workdir \
   cgetools Assembler --sequencing_platform LS454 --sequencing_type single \
   --files "/workdir/services/assembler/test/ls454se.fq.gz"

# Assembly IonTorrent
docker run -ti --rm -w /workdir \
   -v $(pwd):/workdir \
   cgetools Assembler --sequencing_platform 'Ion Torrent' --sequencing_type single \
   --files "/workdir/services/assembler/test/itse.fq.gz"

# ContigAnalyzer
docker run -ti --rm -w /workdir \
   -v $(pwd):/workdir \
   cgetools ContigAnalyzer -f contigs.fsa 

# KmerFinder
docker run -ti --rm -w /workdir \
   -v $dbpath:/databases \
   -v $(pwd):/workdir \
   cgetools KmerFinder -f contigs.fsa -s bacteria

# MLST
docker run -ti --rm -w /workdir \
   -v $dbpath:/databases \
   -v $(pwd):/workdir \
   cgetools MLST -f contigs.fsa -s ecoli

# PlasmidFinder
docker run -ti --rm -w /workdir \
   -v $dbpath:/databases \
   -v $(pwd):/workdir \
   cgetools PlasmidFinder -f contigs.fsa -s enterobacteriaceae -k 80.00

# pMLST
docker run -ti --rm -w /workdir \
   -v $dbpath:/databases \
   -v $(pwd):/workdir \
   cgetools pMLST -f contigs.fsa -s incf

# ResFinder
docker run -ti --rm -w /workdir \
   -v $dbpath:/databases \
   -v $(pwd):/workdir \
   cgetools ResFinder -f contigs.fsa -s phenicol -k 90.00 -l 0.60

# VirulenceFinder
docker run -ti --rm -w /workdir \
   -v $dbpath:/databases \
   -v $(pwd):/workdir \
   cgetools VirulenceFinder -f contigs.fsa -s virulence_ecoli -k 90.00

# SalmonellaTypeFinder
docker run -ti --rm -w /workdir \
   -v $dbpath:/databases \
   -v $(pwd):/workdir \
   cgetools SalmonellaTypeFinder -f R1.fq -f2 R2.fq

# test cgMLSTFinder
# Prepare test database
docker run -ti --rm -w /workdir \
   -v $(pwd)/services/cgmlstfinder/test/:/workdir \
   cgetools /bin/bash -c "tar xzvf campy_test_db.tar.gz && \
            kma_index -i campy_test_db/campy_test_db.fa \
                      -o campy_test_db/campy_test_db"
# Use the test database with test files
docker run -ti --rm -w /workdir \
   -v $(pwd)/services/cgmlstfinder/test/:/databases/cgmlstfinder/ \
   -v $(pwd)/services/cgmlstfinder/test/:/workdir \
   cgetools cgMLSTFinder -f campy_test_R1.fq.gz -f2 campy_test_R2.fq.gz -s campy_test_db

# KMA
docker run -ti --rm -w /workdir \
   -v $dbpath:/databases \
   -v $(pwd):/workdir \
   cgetools kma -f R1.fq -f2 R2.fq --db db.fa

# Bacterial Analysis Pipeline (contigs)
docker run -ti --rm -w /workdir \
   -v $dbpath:/databases \
   -v $(pwd):/workdir \
   cgetools BAP --fa contigs.fsa

# Bacterial Analysis Pipeline (Illumina paired end reads)
docker run -ti --rm -w /workdir \
   -v $dbpath:/databases \
   -v $(pwd):/workdir \
   cgetools BAP --fq1 R1.fq.gz --fq2 R2.fq.gz --Asp Illumina --Ast paired

```

Useful commands
===============
```
# Go to Docker repository
cd /your/path/to/cge-tools-docker

# Update Docker repository
git stash;git pull

# Go to master git branch
git stash;git checkout master

# Shutdown Docker deamon
docker-machine stop default

# Start Docker deamon
docker-machine start default

# Reset Docker env (Used to run docker containers from different terminals)
eval "$(docker-machine env default)"

# Build cgetools Docker image
docker build -t cgetools .

# Docker Cleanup: Remove all dangling/exited containers, images and volumes 
docker container prune
docker image prune
docker volume prune

# Docker Cleanup: Stop and remove all containers (instances of images)
docker rm $(docker stop $(docker ps -aq))

```

Publication
=======
For publications please cite:

A Bacterial Analysis Platform: An Integrated System for Analysing 
Bacterial Whole Genome Sequencing Data for Clinical Diagnostics and 
Surveillance. Martin Christen Fr�lund Thomsen, Johanne Ahrenfeldt, 
Jose Luis Bellod Cisneros, Vanessa Jurtz, Mette Voldby Larsen, 
Henrik Hasman, Frank M�ller Aarestrup, Ole Lund PLoS One. 2016; 
11(6): e0157718


License
=======
Copyright 2016 Center for Genomic Epidemiology, Technical University of Denmark

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
