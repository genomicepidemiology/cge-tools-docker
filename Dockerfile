############################################################
# Dockerfile to build image
############################################################

# Load base Docker image
#FROM python:3.5.3-jessie
#FROM continuumio/anaconda:5.0.1
FROM continuumio/miniconda:4.3.27p0

# Disable Debian frontend interaction promts
ENV DEBIAN_FRONTEND noninteractive

# Fix broken ubuntu repositories
RUN sed -e 's/jessie-updates/stable-updates/' -i /etc/apt/sources.list

# Update the repository sources list
RUN set -ex; \
    apt-get update --fix-missing -qq; \
    apt-get upgrade -qq; \
    apt-get install -y -qq \
        apt-utils=1.0.9.8.5 \
        aufs-tools=1:3.2+20130722-1.1 \
        automake=1:1.14.1-4+deb8u1 \
        blast2=1:2.2.26.20120620-8 \
        btrfs-tools=3.17-1.1 \
        build-essential=11.7 \
        dpkg-sig=0.13.1+nmu2 \
        iptables=1.4.21-2+b1 \
        libapparmor-dev=2.9.0-3 \
        libbz2-dev=1.0.6-7+b3 \
        libcap-dev=1:2.24-8 \
        libfile-slurp-perl=9999.19-4 \
        libjson-perl=2.61-1 \
        liblist-moreutils-perl=0.33-2+b1 \
        liblzma-dev=5.1.1alpha+20120614-2+b3 \
        libmoo-perl=1.006001-1 \
        libmysqlclient-dev=5.5.62-0+deb8u1 \
        libncurses5-dev \
        libsqlite3-dev=3.8.7.1-1+deb8u4 \
        libtbb-dev=4.2~20140122-5 \
        lxc=1:1.0.6-6+deb8u6 \
        make=4.0-8.1 \
        ncbi-blast+=2.2.29-3 \
        openssh-server \
        parallel=20130922-1 \
        pigz=2.3.1-2 \
        python3-pip=1.5.6-5 \
        r-base=3.1.1-1+deb8u1 \
        reprepro=4.16.0-1 \
    ; \
    rm -rf /var/cache/apt/* /var/lib/apt/lists/*;

# Re-enable Debian frontend interaction promts
ENV DEBIAN_FRONTEND Teletype

# Setup ssh
RUN mkdir /root/.ssh/ && \
    ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts && \
    ssh-keyscan cpanmin.us >> /root/.ssh/known_hosts && \
    echo "    IdentityFile ~/.ssh/id_rsa" >> /etc/ssh/ssh_config

# Install External Software Dependencies
#################################################
RUN mkdir -p /usr/src
WORKDIR /usr/src/

# SPADES-3.9.0
RUN wget http://spades.bioinf.spbau.ru/release3.9.0/SPAdes-3.9.0-Linux.tar.gz && \
    tar -xzf SPAdes-3.9.0-Linux.tar.gz && rm SPAdes-3.9.0-Linux.tar.gz
ENV PATH $PATH:/usr/src/SPAdes-3.9.0-Linux/bin

# bwa
RUN wget https://sourceforge.net/projects/bio-bwa/files/bwa-0.7.16a.tar.bz2 && \
    tar xvjf bwa-0.7.16a.tar.bz2 && rm bwa-0.7.16a.tar.bz2 && \
    cd bwa-0.7.16a && make
ENV PATH $PATH:/usr/src/bwa-0.7.16a

# bowtie2
RUN wget https://sourceforge.net/projects/bowtie-bio/files/bowtie2/2.3.3.1/bowtie2-2.3.3.1-source.zip && \
    unzip bowtie2-2.3.3.1-source.zip && rm bowtie2-2.3.3.1-source.zip && \
    cd bowtie2-2.3.3.1 && make
ENV PATH $PATH:/usr/src/bowtie2-2.3.3.1

# samtools
RUN wget https://github.com/samtools/samtools/releases/download/1.6/samtools-1.6.tar.bz2 && \
    tar xvjf samtools-1.6.tar.bz2 && rm samtools-1.6.tar.bz2 && \
    cd samtools-1.6 && make && make install
ENV PATH $PATH:/usr/src/samtools-1.6

# Install perl modules
RUN curl -L https://cpanmin.us -k | perl - App::cpanminus && \
    cpanm Try::Tiny::Retry && \
    cpanm CJFIELDS/BioPerl-1.6.924.tar.gz --force && \
    cpanm Data::Dumper && \
    cpanm Getopt::Long && \
    cpanm File::Temp

# Install Python modules
RUN pip3 install -U numpy==1.14.5 biopython==1.71 tabulate==0.8.2 cgecore==1.3.2
# MySQL-python
# numpy + biopython is a dependency for Assembler
# cgecore is a dependency for all cge pipeline scripts

# Install CGE Tools
#################################################
RUN mkdir -p /usr/src/cgepipeline /usr/src/kma
WORKDIR /usr/src/cgepipeline

# Install the pipeline
#COPY services services
COPY settings settings
COPY cgetools cgetools
COPY test test
COPY setup.py README.md ./

RUN python3 setup.py install --force

# Install kma
#RUN cd /usr/src/kma && \
#    wget -O - https://bitbucket.org/genomicepidemiology/kma/get/master.tar.gz | \
#    tar xzf - --strip-components=1 --wildcards *.c *Makefile && make && \
#    mv kma kma_index kma_shm /usr/local/bin/

# Install kma
RUN git clone --branch 0.14.5 --depth 1 https://bitbucket.org/genomicepidemiology/kma.git; \
    cd kma && make; \
    mv kma kma_index kma_shm /usr/local/bin/

## Configure services
#RUN mv settings/salmonellatypefinder_config_db.txt services/salmonellatypefinder/config_db.txt && \
#    mv settings/salmonellatypefinder_config_prg.txt services/salmonellatypefinder/config_prg.txt
#
## Set execution permissions for scripts
#RUN find services -type f -iname "*.pl" -print -exec chmod 665 {} \; && \
#    find services -type f -iname "*.py" -print -exec chmod 665 {} \;

# Add service scripts to PATH variable
ENV PATH $PATH":/usr/src/cgepipeline/services/assembler/scripts"\
":/usr/src/cgepipeline/services/kmerfinder"\
":/usr/src/cgepipeline/services/resfinder"\
":/usr/src/cgepipeline/services/mlst"\
":/usr/src/cgepipeline/services/pmlst"\
":/usr/src/cgepipeline/services/virulencefinder"\
":/usr/src/cgepipeline/services/plasmidfinder"\
":/usr/src/cgepipeline/services/salmonellatypefinder"\
":/usr/src/cgepipeline/services/cgmlstfinder"\
":/opt/conda/bin"

# Set database location environment variable
ENV CGEPIPELINE_DB /databases

# Set default working directory
WORKDIR /usr/src/cgepipeline/test

# Setup .bashrc file for convenience during debugging
RUN echo "alias ls='ls -h --color=tty'\n"\
"alias ll='ls -lrt'\n"\
"alias l='less'\n"\
"alias du='du -hP --max-depth=1'\n"\
"alias cwd='readlink -f .'\n"\
"PATH=$PATH\n">> ~/.bashrc
